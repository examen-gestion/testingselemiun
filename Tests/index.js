const test1 = require("./Test1.js");
const test2 = require("./Test2.js");
const test3 = require("./Test3.js");
const test4 = require("./Test4.js");
const test5 = require("./Test5.js");
const test6 = require("./Test6.js");
const fs = require('fs');
const path = require('path');

const logFilePath = path.join(__dirname, 'log.txt');

// Redirigir la salida de la consola a un archivo de registro (log file)
const logStream = fs.createWriteStream(logFilePath, { flags: 'a' });
console.log = function (message) {
  logStream.write(`${message}\n`);
  process.stdout.write(`${message}\n`);
};

// Ejecutar los tests y guardar los mensajes en el archivo de registro
(async function main() {
  try {
    console.log('1.Test validate name:');
    await test1();
  } catch (error) {
    console.error(`Error en Test validate name: ${error}`);
    logStream.write(`Error en Test validate name: ${error}\n`);
  }

  try {
    console.log('2.Test validate id_card:');
    await test2();
  } catch (error) {
    console.error(`Error en Test validate id_card: ${error}`);
    logStream.write(`Error en Test validate id_card: ${error}\n`);
  }

  try {
    console.log('3.Test validate phone number letters:');
    await test3();
  } catch (error) {
    console.error(`Error en Test validate phone number letters: ${error}`);
    logStream.write(`Error en Test validate phone number letters: ${error}\n`);
  }

  try {
    console.log('4.Test validate phone number:');
    await test4();
  } catch (error) {
    console.error(`Error en Test validate phone number: ${error}`);
    logStream.write(`Error en Test validate phone number: ${error}\n`);
  }

  try {
    console.log("5.Test email format");
    await test5();
  } catch (error) {
    console.error(`Error en email format: ${error}`);
    logStream.write(`Error en email format: ${error}\n`);
  }

  try {
    console.log("5.Test validation email");
    await test6();
  } catch (error) {
    console.error(`Error en validation email: ${error}`);
    logStream.write(`Error en validation email: ${error}\n`);
  }

  console.log('Execution end');
})();