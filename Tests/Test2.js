const {Builder, Browser, By, Key, until, WebDriver} = require('selenium-webdriver');
require ('geckodriver');
const assert = require("assert");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//2 TEST -> EL ESPACIO CÉDULA SOLAMENTE ACEPTA DÍGITOS CON UNA LONGITUD DE 9 DÍGITOS
async function validateIdCard(){

    let driver =  await  new Builder().forBrowser(Browser.FIREFOX).build();

    await driver.get("http://localhost:3000/patient/create");

    console.log("TEST #2 -> Ingreso de menor cantidad de dígitos al campo cédula");

    await driver.findElement(By.name("id_card")).sendKeys("706840", Key.RETURN);
    let error_idCard = await driver.findElement(By.name("error_idCard")).getText();
    console.log("Mensaje esperado: ",error_idCard);
    
    sleep(500);
    
    assert.equal(error_idCard, "Solo se permiten 9 dígitos")
    console.log("Assert correcto")

    sleep(500);

    let element = driver.findElement(By.name("comments"));
    driver.executeScript("arguments[0].scrollIntoView();", element);

    await driver.findElement(By.name("btn_add")).click();

    await driver.quit();
}
module.exports = validateIdCard;
