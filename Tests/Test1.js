const {Builder, Browser, By, Key, until, WebDriver} = require('selenium-webdriver');
require ('geckodriver');
const assert = require("assert");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//1 TEST -> EL ESPACIO NOMBRE SOLAMENTE ACEPTA LETRAS
async function validateName(){

    let driver =  await  new Builder().forBrowser(Browser.FIREFOX).build();

    await driver.get("http://localhost:3000/patient/create");

    console.log("TEST #1 -> Ingreso de números y símbolos al campo nombre");

    await driver.findElement(By.name("name")).sendKeys("Isa12#*", Key.RETURN);
    let error_name = await driver.findElement(By.name("error_name")).getText();
    console.log("Mensaje esperado: ", error_name);

    sleep(50000);

    assert.equal(error_name, "Solo se aceptan letras")
    console.log("Assert correcto")

    sleep(50000);

    //BUSCA UN ELEMENTO Y BAJA EL SCROLL
    let element = driver.findElement(By.name("comments"));
    driver.executeScript("arguments[0].scrollIntoView();", element);

    await driver.findElement(By.name("btn_add")).click();

    await driver.quit();
}
module.exports = validateName;
