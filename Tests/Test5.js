const {Builder, Browser, By, Key, until, WebDriver} = require('selenium-webdriver');
require ('geckodriver');
const assert = require("assert");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//5 TEST -> EL ESPACIO CORREO SOLAMENTE ACEPTA FORMATO EMAIL (example@domain.com) Y ES UN CAMPO REQUERIDO
async function validateEmailFormat(){

    let driver =  await  new Builder().forBrowser(Browser.FIREFOX).build();

    await driver.get("http://localhost:3000/patient/create");

    console.log("TEST #5 -> Ingreso del correo con formato incorrecto");

    await driver.findElement(By.name("email")).sendKeys("mariolgmail.com", Key.RETURN);
    let error_email = await driver.findElement(By.name("error_email")).getText();
    console.log("Mensaje esperado: ",error_email);
    
    sleep(50000);
    
    assert.equal(error_email, "Formato inválido")
    console.log("Assert correcto")

    sleep(50000);

    let element = driver.findElement(By.name("comments"));
    driver.executeScript("arguments[0].scrollIntoView();", element);

    await driver.findElement(By.name("btn_add")).click();

    await driver.quit();
}

module.exports = validateEmailFormat;
