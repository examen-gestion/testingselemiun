const {Builder, Browser, By, Key, until, WebDriver} = require('selenium-webdriver');
require ('geckodriver');
const assert = require("assert");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//4 TEST -> EL ESPACIO TÉLEFONO SOLAMENTE ACEPTA DÍGITOS CON LONGITUD DE 8 DÍGITOS
async function validatePhoneNumberFormat(){

    let driver =  await  new Builder().forBrowser(Browser.FIREFOX).build();

    await driver.get("http://localhost:3000/patient/create");

    console.log("TEST #4 -> Ingreso de mayor cantidad de dígitos al campo télefono");

    await driver.findElement(By.name("phone_number")).sendKeys("8756342190", Key.RETURN);
    let error_phoneNumber = await driver.findElement(By.name("error_phoneNumber")).getText();
    console.log("Mensaje esperado: ",error_phoneNumber);

    sleep(500);
    
    assert.equal(error_phoneNumber, "Solo se permiten 8 dígitos")
    console.log("Assert correcto")

    sleep(500);

    let element = driver.findElement(By.name("comments"));
    driver.executeScript("arguments[0].scrollIntoView();", element);

    await driver.findElement(By.name("btn_add")).click();

    await driver.quit();
}

module.exports = validatePhoneNumberFormat;
